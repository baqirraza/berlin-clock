package com.inkglobal.techtest;

import org.junit.Test;

public class BerlinClockTest {

    BerlinClock berlinClock = new BerlinClock();
    
    // Berlin Clock should result in array with 5 elements
    @Test
    public void testBerlinClockShouldResultInArrayWith5Elements()  {
        Assert.assertEquals(5, berlinClock.convertToBerlinTime("13:17:01").length);
    }
 
    // Berlin Clock it should "result in correct seconds, hours and minutes" in the Array
    @Test
    public void testBerlinClockShouldResultInCorrectSecondsHoursAndMinutes() {
    	
    	String timeGiven = "00:00:00";
    	String[] expected = new String[] {"Y", "OOOO", "OOOO", "OOOOOOOOOOO", "OOOO"};
    	
        String[] berlinTime = berlinClock.convertToBerlinTime(timeGiven);
        Assert.assertEquals(expected.length, berlinTime.length);
        for (int index = 0; index < expected.length; index++) {
            Assert.assertEquals(expected[index], berlinTime[index]);
        }
        
        timeGiven = "13:17:01";
        expected = new String[] {"O", "RROO", "RRRO", "YYROOOOOOOO", "YYOO"};
        
        berlinTime = berlinClock.convertToBerlinTime(timeGiven);
        Assert.assertEquals(expected.length, berlinTime.length);
        for (int index = 0; index < expected.length; index++) {
            Assert.assertEquals(expected[index], berlinTime[index]);
        }
        
        timeGiven = "23:59:59";
        expected = new String[] {"O", "RRRR", "RRRO", "YYRYYRYYRYY", "YYYY"};
        
        berlinTime = berlinClock.convertToBerlinTime(timeGiven);
        Assert.assertEquals(expected.length, berlinTime.length);
        for (int index = 0; index < expected.length; index++) {
            Assert.assertEquals(expected[index], berlinTime[index]);
        }
        
        timeGiven = "24:00:00";
        expected = new String[] {"Y", "RRRR", "RRRR", "OOOOOOOOOOO", "OOOO"};
        
        berlinTime = berlinClock.convertToBerlinTime(timeGiven);
        Assert.assertEquals(expected.length, berlinTime.length);
        for (int index = 0; index < expected.length; index++) {
            Assert.assertEquals(expected[index], berlinTime[index]);
        }
        
    }
}
