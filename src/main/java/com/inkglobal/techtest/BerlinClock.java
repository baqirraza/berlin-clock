package com.inkglobal.techtest;

public class BerlinClock {

	public String[] convertToBerlinTime(String time){
		
    	int hour = Integer.parseInt(time.split(":")[0]);
    	int minute = Integer.parseInt(time.split(":")[1]);
    	int second = Integer.parseInt(time.split(":")[2]);
    	
		String berlinTime[] = new String[] {getSecondsIndicator(second), getFirstHoursRow(hour), 
				getSecondHoursRow(hour), getFirstMinutesRow(minute), getSecondMinutesRow(minute)};
		
		return berlinTime;
	}
	
    protected String getSecondsIndicator(int number) {
    	// The top indicator switches On or Off every two seconds
    	return (number % 2 == 0)?"Y":"O";
    }

    protected String getFirstHoursRow(int number) {
    	
    	// Get the top number of on signs 
    	int topOnLamps = getTopNumberOfOnLamps(number);
    	
    	// Top row has 4 lamps
    	String topHoursRow = getOnOff(4, topOnLamps, "R");
    	
        return topHoursRow;
    }

    protected String getSecondHoursRow(int number) {
    	
    	// The bottom hours row also has 4 lamps
    	// With each lamp representing 1 hour
    	// These will be switched on for the remainder of 5 hours
    	String bottomHoursRow = getOnOff(4, number % 5, "R");
    	
        return bottomHoursRow;
    }

    protected String getFirstMinutesRow(int number) {
    	
    	// Get the top number of on signs 
    	int topOnSigns = getTopNumberOfOnLamps(number);

    	// The raw OnOff list of lamps
    	// Minute row has 11 lamps
    	String onOff = getOnOff(11, topOnSigns, "Y");
    	
    	// Note that the 3rd, 6th, and 9th lamps are Red, i.e., every third lamp is red
    	onOff = onOff.replaceAll("YYY", "YYR");
    	
        return onOff;
    }

    protected String getSecondMinutesRow(int number) {
    	
    	// The bottom minutes row also has 4 lamps
    	// With each lamp representing 1 minute
    	// These will be switched on for the remainder of 5 minutes
    	String bottomMinutesRow = getOnOff(4, number % 5, "Y");
    	
        return bottomMinutesRow;
    }

    private String getOnOff(int lamps, int onSigns, String onSign) {
    	
    	StringBuilder out = new StringBuilder();
    	
    	// First create all lamps with default value
    	for (int i = 0; i < lamps; i++) {
    		out.append("O");
		}
    	
    	// Now switch on the lamps
    	for (int i = 0; i < onSigns; i++) {
    		out.setCharAt(i, onSign.charAt(0));
		}
        return out.toString();
    }
    
    // Each lamp in the TOP row represent 5 hours
    private int getTopNumberOfOnLamps(int number) {
        return (number - (number % 5)) / 5;
    }
}
