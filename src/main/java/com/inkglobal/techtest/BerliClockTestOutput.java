package com.inkglobal.techtest;

public class BerliClockTestOutput {

	public static void main (String a[]){
		
		String timeToConvert = "13:47:01";
		BerlinClock berlinClock = new BerlinClock();
		String[] time = berlinClock.convertToBerlinTime(timeToConvert);

		for (String string : time) {
			System.out.println(string);
		}
	}
}
